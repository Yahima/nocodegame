﻿using Mirror;
using UnityEngine;

namespace Edgar.Unity.Examples
{
    /// <summary>
    /// Basic camera controller that follows the player and zooms out if you hold CTRL.
    /// </summary>
    public class CameraController : MonoBehaviour
    {
        public float ZoomOutSize = 70;

        private new Camera camera;
        private GameObject player;

        private bool isZoomedOut;
        private float previousOrthographicSize;
        private Vector3 previousPosition;

        public void Start()
        {
            camera = GetComponent<Camera>();
        }

        public void Update()
        {
            if (InputHelper.GetKeyDown(KeyCode.LeftControl))
            {
                previousOrthographicSize = camera.orthographicSize;
                camera.orthographicSize = ZoomOutSize;

                previousPosition = transform.position;
                transform.position = new Vector3(0, 0, previousPosition.z);
                isZoomedOut = true;
            }

            if (InputHelper.GetKeyUp(KeyCode.LeftControl))
            {
                camera.orthographicSize = previousOrthographicSize;
                transform.position = previousPosition;
                isZoomedOut = false;
            }
        }

        public void LateUpdate()
        {
            if (player == null)
            {
                var localPlayer = NetworkClient.localPlayer;
                if (localPlayer != null && localPlayer.CompareTag("Player"))
                {
                    player = localPlayer.gameObject;
                }
            }

            if (player != null && !isZoomedOut)
            {
                var playerPosition = player.transform.position;
                transform.position = new Vector3(playerPosition.x, playerPosition.y, transform.position.z);
            }
        }
    }
}