﻿using System;
using System.Collections;
using System.Diagnostics;
using Mirror;
using UnityEngine;
using Debug = UnityEngine.Debug;
using Random = System.Random;

namespace Edgar.Unity.Examples.Gungeon
{
    /// <summary>
    /// Example of a simple game manager that uses the DungeonGeneratorRunner to generate levels.
    /// </summary>
    public class GungeonGameManager : GameManagerBase<GungeonGameManager>
    {
        [Range(1, 2)]
        public int Stage = 1;
        public LevelGraph CurrentLevelGraph;

        public Random Random;
        
        private bool _isGenerating;
        private long _generatorElapsedMilliseconds;

        public static Action GenerateNewEvent;
        public static Action<int> OpenDoorEvent;

        public DungeonGeneratorPayloadGrid2D GeneratorPayload { get; private set; }
        private RoomInstanceGrid2D _currentRoom;
        private RoomInstanceGrid2D _nextCurrentRoom;

        protected override void SingletonAwake()
        {
            NetworkClient.RegisterHandler<GenerateNewMessage>(OnGenerateNewMessage);
            NetworkClient.RegisterHandler<OpenDoorMessage>(OnClientOpenDoorMessage);
            
            NetworkServer.RegisterHandler<OpenDoorMessage>(OnServerOpenDoorMessage);
        }

        public void Update()
        {
            // if (InputHelper.GetKeyDown(KeyCode.G) && !_isGenerating)
            // {
            //     LoadNextLevel();
            // }
            //
            // if (InputHelper.GetKeyDown(KeyCode.H) && !_isGenerating)
            // {
            //     Stage = Stage == 1 ? 2 : 1;
            //     LoadNextLevel();
            // }
        }

        private void OnGenerateNewMessage(GenerateNewMessage msg)
        {
            _isGenerating = true;

            // Show loading screen
            ShowLoadingScreen("Gungeon", $"Stage {Stage}");

            GenerateNewEvent?.Invoke();
            
            // Find the generator runner
            var generator = GameObject.Find("Dungeon Generator").GetComponent<DungeonGeneratorGrid2D>();

            generator.UseRandomSeed = false;
            generator.RandomGeneratorSeed = msg.Seed;
            // Start the generator coroutine
            StartCoroutine(GeneratorCoroutine(generator));
        }

        public override void LoadNextLevel()
        {
            // NetManager.LoadNextLevel();
            // NetworkServer.SendToReady(new RestartMessage(_seedsGenerator.Next()));
        }

        /// <summary>
        /// Coroutine that generates the level.
        /// We need to yield return before the generator starts because we want to show the loading screen
        /// and it cannot happen in the same frame.
        /// It is also sometimes useful to yield return before we hide the loading screen to make sure that
        /// all the scripts that were possibly created during the process are properly initialized.
        /// </summary>
        private IEnumerator GeneratorCoroutine(DungeonGeneratorGrid2D generator)
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();

            // Configure the generator with the current stage number
            var inputTask = (GungeonInputSetupTask) generator.CustomInputTask;
            inputTask.Stage = Stage;

            var generatorCoroutine = this.StartSmartCoroutine(generator.GenerateCoroutine(), true);

            yield return generatorCoroutine.Coroutine;
            yield return null;

            stopwatch.Stop();
            _isGenerating = false;
            _generatorElapsedMilliseconds = stopwatch.ElapsedMilliseconds;

            generatorCoroutine.ThrowIfNotSuccessful();

            GeneratorPayload = generator.CachedPayload;
            // Debug.LogError($"value = {generator.CachedPayload.GeneratedLevel.RoomInstances.Count}");

            _currentRoom = null;
            _nextCurrentRoom = null;
            RefreshLevelInfo();

            var rootGameObject = generator.GeneratorConfig.RootGameObject;
            
            var netId = rootGameObject.GetComponent<NetworkIdentity>();
            netId.InitializeNetworkBehaviours();

            foreach (var roomManager in rootGameObject.GetComponentsInChildren<GungeonRoomManager>())
            {
                roomManager.SpawnItems();
            }
            
            NetworkClient.Send(new GenerationDoneMessage());
            
            HideLoadingScreen();
        }

        private void RefreshLevelInfo()
        {
            var info = $"Generated in {_generatorElapsedMilliseconds / 1000d:F}s\n";
            info += $"Stage: {Stage}, Level graph: {CurrentLevelGraph.name}\n";
            info += $"Room type: {(_currentRoom?.Room as GungeonRoom)?.Type}, Room template: {_currentRoom?.RoomTemplatePrefab.name}";

            SetLevelInfo(info);
        }

        public void OnRoomEnter(RoomInstanceGrid2D roomInstance, GameObject player)
        {
            if (!player.GetComponent<NetworkIdentity>().isLocalPlayer)
            {
                return;
            }
            
            _nextCurrentRoom = roomInstance;

            if (_currentRoom == null)
            {
                _currentRoom = _nextCurrentRoom;
                _nextCurrentRoom = null;
                RefreshLevelInfo();
            }
        }

        public void OnRoomLeave(RoomInstanceGrid2D roomInstance, GameObject player)
        {
            if (!player.GetComponent<NetworkIdentity>().isLocalPlayer)
            {
                return;
            }
            
            _currentRoom = _nextCurrentRoom;
            _nextCurrentRoom = null;
            RefreshLevelInfo();
        }
        
        public Vector3 GetPlayerSpawnPos()
        {
            var level = GeneratorPayload.GeneratedLevel;
        
            foreach (var roomInstance in level.RoomInstances)
            {
                var room = (GungeonRoom) roomInstance.Room;
                var roomTemplateInstance = roomInstance.RoomTemplateInstance;

                // Get spawn position if Entrance
                if (room.Type == GungeonRoomType.Entrance)
                {
                    var spawnPosition = roomTemplateInstance.transform.Find("SpawnPosition");
                    return spawnPosition.position;
                }
            }

            return Vector3.zero;
        }

        public void OpenDoor(int doorId)
        {
            NetworkClient.Send(new OpenDoorMessage { DoorId = doorId });
        }

        private void OnServerOpenDoorMessage(NetworkConnectionToClient conn, OpenDoorMessage msg)
        {
            NetworkServer.SendToReady(msg);
        }
        
        private void OnClientOpenDoorMessage(OpenDoorMessage msg)
        {
            OpenDoorEvent?.Invoke(msg.DoorId);
        }
    }
    
    public struct GenerateNewMessage : NetworkMessage //наследуемся от интерфейса NetworkMessage, чтобы система поняла какие данные упаковывать
    {
        public int Seed;

        public GenerateNewMessage(int seed)
        {
            Seed = seed;
        }
        // public Vector2 vector2; //нельзя использовать Property
    }
    
    public struct GenerationDoneMessage : NetworkMessage {}

    public struct OpenDoorMessage : NetworkMessage
    {
        public int DoorId;
    }
}