using UnityEngine;

namespace Edgar.Unity.Examples.Gungeon
{
    public class GungeonItemPlacer : MonoBehaviour
    {
        [SerializeField]
        private GameObject _itemPrefab;

        public GameObject ItemPrefab => _itemPrefab;
    }
}