using System.Collections.Generic;
using Character;
using Mirror;
using TopDownCharacter2D.Stats;
using UnityEngine;

namespace Items
{
    /// <summary>
    ///     Handles the logic for a stat modifier pickup item
    /// </summary>
    public class NetPickupStatModifiers : NetPickupItem
    {
        [Tooltip("The stats modifier added to the character after this item is picked up")]
        [SerializeField] private List<CharacterStats> _statsModifier;

        [ClientRpc]
        protected override void OnPickedUp(GameObject receiver)
        {
            NetCharacterStatsHandler netStatsHandler = receiver.gameObject.GetComponent<NetCharacterStatsHandler>();
            if (netStatsHandler != null)
            {
                foreach (CharacterStats stat in _statsModifier)
                {
                    netStatsHandler.StatsModifiers.Add(stat);
                }
            }
        }
    }
}