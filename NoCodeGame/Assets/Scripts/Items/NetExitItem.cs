using Edgar.Unity.Examples;
using Edgar.Unity.Examples.Example2;
using Mirror;
using UnityEngine;

namespace Items
{
    public class NetExitItem : InteractableBase
    {
        public override void BeginInteract()
        {
            ShowText("Press E to go deeper");
        }

        public override void Interact()
        {
            if (InputHelper.GetKey(KeyCode.E))
            {
                gameObject.SetActive(false);
                NetworkClient.Send(new NextLevelMessage());
            }
        }

        public override void EndInteract()
        {
            HideText();
        }
    }
}