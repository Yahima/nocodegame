using System;
using System.Collections;
using Edgar.Unity.Examples.Gungeon;
using Mirror;
using UnityEngine;
using UnityEngine.Events;

namespace Items
{
    /// <summary>
    ///     Handles the logic of a pickup item
    /// </summary>
    public abstract class NetPickupItem : NetworkBehaviour
    {
        [Tooltip("If the pickup item must be destroyed or not after having been picked up")]
        [SerializeField] private bool destroyOnPickup = true;
        
        [Tooltip("The layer of the objects that can pick up this item")]
        [SerializeField] private LayerMask canBePickupBy;

        public UnityEvent OnPickup { get; } = new UnityEvent();

        public void Awake()
        {
            GungeonGameManager.GenerateNewEvent += OnGenerateNewEvent;
        }
        
        private void OnDestroy()
        {
            GungeonGameManager.GenerateNewEvent -= OnGenerateNewEvent;
        }

        /// <summary>
        ///     If the item collides with a player, give its effect to the player and disappear itself
        /// </summary>
        /// <param name="other"> The other collider </param>
        private void OnTriggerEnter2D(Collider2D other)
        {
            if (canBePickupBy.value == (canBePickupBy.value | (1 << other.gameObject.layer)))
            {
                ServerPickUp(other.gameObject);
            }
        }

        [Server]
        private void ServerPickUp(GameObject receiver)
        {
            OnPickedUp(receiver);
            OnPickup.Invoke();
            if (destroyOnPickup)
            {
                DestroyItem();
                StartCoroutine(DelayedDestroy());
            }
        }

        /// <summary>
        ///     Called when the item gets picked up
        /// </summary>
        /// <param name="receiver"> The GameObject receiving the effect </param>
        [ClientRpc]
        protected virtual void OnPickedUp(GameObject receiver)
        {
        }

        /// <summary>
        ///     Destroy the item while allowing it to finish its effects.
        /// </summary>
        [ClientRpc]
        private void DestroyItem()
        {
            foreach (Behaviour component in transform.GetComponentsInChildren<Behaviour>())
            {
                if (!(component is AudioSource))
                {
                    component.enabled = false;
                }
            }

            foreach (Renderer component in transform.GetComponentsInChildren<Renderer>())
            {
                if (!(component is ParticleSystemRenderer))
                {
                    component.enabled = false;
                }
            }

            Destroy(gameObject, 5f);
        }
        
        private void OnGenerateNewEvent()
        {
            StopAllCoroutines();
            
            NetworkServer.Destroy(gameObject);
        }

        private IEnumerator DelayedDestroy()
        {
            yield return new WaitForSecondsRealtime(5f);
            
            NetworkServer.Destroy(gameObject);
        }
    }
}