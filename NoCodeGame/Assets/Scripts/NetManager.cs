using Analytics;
using Edgar.Unity.Examples.Gungeon;
using Mirror;
using Sentry;
using UnityEngine;
using Random = System.Random;

public class NetManager : NetworkManager
{
    [SerializeField] 
    private GameObject _playerDummy;
    
    private static readonly Random SeedsGenerator = new Random();
    
    bool playerSpawned;
    bool playerConnected;
    
    private int _requiredPlayers = 2;
    private int _generationReadyPlayers;
    
    public override void Update()
    {
        base.Update();
        
        if (Input.GetKeyDown(KeyCode.G) && NetworkServer.active)
        {
            _requiredPlayers = NetworkServer.connections.Count;
            TryStartDungeon();
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
            SentrySdk.CaptureMessage("Test event");
        }
    }

    public override void OnStartHost()
    {
        base.OnStartHost();
        
        TrackerManager.Create(TrackEventName.StartHost)
            .Flush();
    }

    public override void OnStartClient()
    {
        base.OnStartClient();
        NetworkClient.RegisterPrefab(_playerDummy);
        
        TrackerManager.Create(TrackEventName.StartClient)
            .Flush();
    }

    public override void OnClientConnect()
    {
        base.OnClientConnect();
        playerConnected = true;
    }

    public override void OnClientSceneChanged()
    {
        base.OnClientSceneChanged();
        if (Utils.IsSceneActive("GameScene"))
        {
            GungeonGameManager.Instance.ShowLoadingScreen("Waiting for players...", "");
        }
    }
    
    public override void OnServerSceneChanged(string sceneName)
    {
        base.OnServerSceneChanged(sceneName);
        if (Utils.IsSceneActive("GameScene"))
        {
            GungeonGameManager.Instance.ShowLoadingScreen("Waiting for players...", "");
        }
    }

    public override void OnStartServer()
    {
        base.OnStartServer();
        
        NetworkServer.RegisterHandler<GenerationDoneMessage>(OnGenerationDoneMessage);
        NetworkServer.RegisterHandler<NextLevelMessage>(OnNextLevelMessage);
        
        TrackerManager.Create(TrackEventName.StartServer)
            .Flush();
    }

    public override void OnServerReady(NetworkConnectionToClient conn)
    {
        var playerDummy = Instantiate(_playerDummy);
        NetworkServer.AddPlayerForConnection(conn, playerDummy);
        
        TryStartDungeon();
    }

    private void TryStartDungeon()
    {
        if (NetworkServer.connections.Count >= _requiredPlayers)
        {
            GenerateNewDungeon();
        }
    }

    private void GenerateNewDungeon()
    {
        _generationReadyPlayers = 0;
        NetworkServer.SendToReady(new GenerateNewMessage(SeedsGenerator.Next()));
    }
    
    private void OnGenerationDoneMessage(NetworkConnectionToClient conn, GenerationDoneMessage msg)
    {
        _generationReadyPlayers++;

        if (_generationReadyPlayers >= _requiredPlayers)
        {
            var position = GungeonGameManager.Instance.GetPlayerSpawnPos();

            foreach (var connection in NetworkServer.connections.Values)
            {
                NetworkServer.RemovePlayerForConnection(connection, true);

                var player = Instantiate(playerPrefab, position, Quaternion.identity);
                NetworkServer.AddPlayerForConnection(connection, player);
            }
        }
    }
    
    private void OnNextLevelMessage(NetworkConnectionToClient conn, NextLevelMessage msg)
    {
        TryStartDungeon();
    }
}

public struct NextLevelMessage : NetworkMessage {}
