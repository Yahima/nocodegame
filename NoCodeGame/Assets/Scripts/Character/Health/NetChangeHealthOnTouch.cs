using Mirror;
using TopDownCharacter2D;
using TopDownCharacter2D.Health;
using UnityEngine;

namespace Character.Health
{
    /// <summary>
    ///     This script allows an entity to deal contact damage to other entities with a given tag
    /// </summary>
    public class NetChangeHealthOnTouch : NetworkBehaviour
    {
        [Tooltip("The health change on contact (must be negative for damages)")] 
        [SerializeField]
        private float _value;

        [Tooltip("The tag of the target of the health change")]
        [SerializeField]
        private string _targetTag;

        private NetHealthSystem _collidingTargetHealthSystem;
        private TopDownKnockBack _collidingTargetKnockBackSystem;

        private bool _isCollidingWithTarget;

        private void FixedUpdate()
        {
            if (!isServer)
            {
                return;
            }
            
            if (_isCollidingWithTarget)
            {
                ApplyHealthChange();
            }
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            GameObject receiver = collision.gameObject;

            if (!receiver.CompareTag(_targetTag))
            {
                return;
            }

            _collidingTargetHealthSystem = receiver.GetComponent<NetHealthSystem>();
            if (_collidingTargetHealthSystem != null)
            {
                _isCollidingWithTarget = true;
            }

            _collidingTargetKnockBackSystem = receiver.GetComponent<TopDownKnockBack>();
        }


        private void OnTriggerExit2D(Collider2D collision)
        {
            if (!collision.CompareTag(_targetTag))
            {
                return;
            }

            _isCollidingWithTarget = false;
        }

        /// <summary>
        ///     Apply the change of health to the target
        /// </summary>
        private void ApplyHealthChange()
        {
            var pos = transform.position;
            _collidingTargetHealthSystem.ChangeHealth(_value, pos);
            
            // bool hasBeenChanged = _collidingTargetHealthSystem.ChangeHealth(_value);
            //
            // if (_collidingTargetKnockBackSystem != null && hasBeenChanged)
            // {
            //     _collidingTargetKnockBackSystem.ApplyKnockBack(transform);
            // }
        }
    }
}