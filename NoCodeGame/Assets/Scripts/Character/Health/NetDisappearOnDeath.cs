using System;
using System.Collections;
using Edgar.Unity.Examples.Gungeon;
using Mirror;
using TopDownCharacter2D.FX;
using UnityEngine;

namespace Character.Health
{
    public class NetDisappearOnDeath : NetworkBehaviour
    {
        public void Awake()
        {
            GungeonGameManager.GenerateNewEvent += OnGenerateNewEvent;
        }
        
        private void OnDestroy()
        {
            GungeonGameManager.GenerateNewEvent -= OnGenerateNewEvent;
        }

        public void OnDeath()
        {
            foreach (Behaviour component in transform.GetComponentsInChildren<Behaviour>())
            {
                if (!(component is AudioSource) && !(component is TopDownFx))
                {
                    component.enabled = false;
                }
            }

            foreach (Renderer component in transform.GetComponentsInChildren<Renderer>())
            {
                if (!(component is ParticleSystemRenderer))
                {
                    component.enabled = false;
                }
            }

            if (isServer)
            {
                StartCoroutine(DelayedDestroy());
            }
            // We wait before destroying the object in order to properly end all the related effects
            // Destroy(gameObject, 20f);
        }
        
        private void OnGenerateNewEvent()
        {
            StopAllCoroutines();
            
            NetworkServer.Destroy(gameObject);
        }
        
        private IEnumerator DelayedDestroy()
        {
            yield return new WaitForSecondsRealtime(20f);
            
            NetworkServer.Destroy(gameObject);
        }
    }
}