using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Character.Health
{
    public class NetHealthGaugeUpdater : MonoBehaviour
    {
        [SerializeField] private Slider healthSlider;
        [SerializeField] private GameObject entityObject;

        [SerializeField] private UnityEvent onHealthUpdate;

        private NetHealthSystem _entityHealth;

        private void Awake()
        {
            _entityHealth = entityObject.GetComponent<NetHealthSystem>();
        }

        private void Start()
        {
            _entityHealth.OnDamage.AddListener(UpdateHealth);
            _entityHealth.OnHeal.AddListener(UpdateHealth);
        }

        /// <summary>
        ///     Updates the health bar slider's value
        /// </summary>
        private void UpdateHealth()
        {
            healthSlider.value = _entityHealth.CurrentHealth / _entityHealth.MaxHealth;
            onHealthUpdate.Invoke();
        }
    }
}