using Mirror;
using TopDownCharacter2D;
using UnityEngine;
using UnityEngine.Events;

namespace Character.Health
{
    /// <summary>
    ///     Handles the health of an entity
    /// </summary>
    public class NetHealthSystem : NetworkBehaviour
    {
        [Tooltip("The delay between two health changes in seconds")]
        [SerializeField] private float healthChangeDelay = .5f;

        [SerializeField] private UnityEvent onDamage;
        [SerializeField] private UnityEvent onHeal;
        [SerializeField] private UnityEvent onDeath;
        [SerializeField] private UnityEvent onInvincibilityEnd;

        private NetCharacterStatsHandler _statsHandler;
        private float _timeSinceLastChange = float.MaxValue;

        public UnityEvent OnDamage => onDamage;
        public UnityEvent OnHeal => onHeal;
        public UnityEvent OnDeath => onDeath;
        public UnityEvent OnInvincibilityEnd => onInvincibilityEnd;

        // [SyncVar]
        public float CurrentHealth;

        public float MaxHealth => _statsHandler.CurrentStats.maxHealth;

        private void Awake()
        {
            _statsHandler = GetComponent<NetCharacterStatsHandler>();
        }

        private void Start()
        {
            CurrentHealth = _statsHandler.CurrentStats.maxHealth;
        }

        private void Update()
        {
            if (_timeSinceLastChange < healthChangeDelay)
            {
                _timeSinceLastChange += Time.deltaTime;
                if (_timeSinceLastChange >= healthChangeDelay)
                {
                    onInvincibilityEnd.Invoke();
                }
            }
        }

        /// <summary>
        ///     Modifies the health of the entity
        /// </summary>
        /// <param name="change"> The amount of health to add</param>
        /// <returns></returns>
        [ClientRpc]
        public void ChangeHealth(float change, Vector3 pos)
        {
            if (change == 0 || _timeSinceLastChange < healthChangeDelay)
            {
                return;
            }

            _timeSinceLastChange = 0f;
            CurrentHealth += change;
            CurrentHealth = CurrentHealth > MaxHealth ? MaxHealth : CurrentHealth;
            CurrentHealth = CurrentHealth < 0 ? 0 : CurrentHealth;

            if (change > 0)
            {
                onHeal.Invoke();
            }
            else
            {
                OnDamage.Invoke();
                if (isOwned)
                {
                    TopDownKnockBack knockBack = gameObject.GetComponent<TopDownKnockBack>();
                    if (knockBack != null)
                    {
                        knockBack.ApplyKnockBack(pos);
                    }
                }
            }

            if (CurrentHealth <= 0f)
            {
                Death();
            }

            // return true;
        }

        private void Death()
        {
            onDeath.Invoke();
        }
    }
}