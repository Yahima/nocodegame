using Character.Controllers;
using Mirror;
using UnityEngine;

namespace Character
{
    /// <summary>
    ///     This class contains the logic for movement in a 2D environment with a top down view
    /// </summary>
    [RequireComponent(typeof(NetCharacterStatsHandler))]
    [RequireComponent(typeof(Rigidbody2D))]
    [RequireComponent(typeof(NetTopDownCharacterController))]
    public class NetTopDownMovement : NetworkBehaviour
    {
        private NetTopDownCharacterController _controller;

        private Vector2 _movementDirection = Vector2.zero;
        private Rigidbody2D _rb;
        private NetCharacterStatsHandler _stats;

        private void Awake()
        {
            _controller = GetComponent<NetTopDownCharacterController>();
            _stats = GetComponent<NetCharacterStatsHandler>();
            _rb = GetComponent<Rigidbody2D>();
        }

        private void Start()
        {
            _controller.OnMoveEvent.AddListener(Move);
        }

        private void FixedUpdate()
        {
            ApplyMovement(_movementDirection);
        }

        /// <summary>
        ///     Changes the current direction of the movement
        /// </summary>
        /// <param name="direction"></param>
        private void Move(Vector2 direction)
        {
            _movementDirection = direction;
        }

        /// <summary>
        ///     Used to apply a given movement vector to the rigidbody of the character
        /// </summary>
        /// <param name="direction"> The direction in which to move the rigidbody </param>
        private void ApplyMovement(Vector2 direction)
        {
            _rb.velocity += direction * _stats.CurrentStats.speed;
        }
    }
}