using Character.Controllers;
using Mirror;
using UnityEngine;

namespace Character.Animations
{
    /// <summary>
    ///     This class contains the animation logic for a 2D sprite in a top down view
    /// </summary>
    [RequireComponent(typeof(Animator))]
    [RequireComponent(typeof(NetTopDownCharacterController))]
    public abstract class NetTopDownAnimations : NetworkBehaviour
    {
        protected Animator animator;
        protected NetTopDownCharacterController controller;
        
        protected virtual void Awake()
        {
            animator = GetComponent<Animator>();
            controller = GetComponent<NetTopDownCharacterController>();
        }
    }
}