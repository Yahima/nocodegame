using Character.Health;
using Mirror;
using TopDownCharacter2D.Attacks;
using UnityEngine;

namespace Character.Animations
{
    public class NetSampleCharacterAnimation : NetTopDownAnimations
    {
        private static readonly int IsWalking = Animator.StringToHash("IsWalking");
        private static readonly int Attack = Animator.StringToHash("Attack");
        private static readonly int IsHurt = Animator.StringToHash("IsHurt");
        
        [SerializeField] private bool createDustOnWalk = true;
        [SerializeField] private ParticleSystem dustParticleSystem;
        
        private NetHealthSystem _healthSystem;
        
        protected override void Awake()
        {
            base.Awake();
            _healthSystem = GetComponent<NetHealthSystem>();
        }

        protected void Start()
        {
            controller.OnMoveEvent.AddListener(OnMoveEvent);
            controller.OnAttackEvent.AddListener(OnAttackEvent);

            if (_healthSystem != null)
            {
                _healthSystem.OnDamage.AddListener(PlayHurt);
                _healthSystem.OnInvincibilityEnd.AddListener(OnInvincibilityEnd);
            }
        }

        /// <summary>
        ///     To call when the character moves, change the animation to the walking one
        /// </summary>
        /// <param name="movementDirection"> The new movement direction </param>
        private void OnMoveEvent(Vector2 movementDirection)
        {
            animator.SetBool(IsWalking, movementDirection.magnitude > .5f);
        }

        private void OnAttackEvent(AttackConfig attackConfig)
        {
            if (isServer)
            {
                PlayAttacking();
            }
            else
            {
                CmdPlayAttacking();
            }
        }

        [Command]
        private void CmdPlayAttacking()
        {
            PlayAttacking();
        }

        /// <summary>
        ///     To call when the character attack
        /// </summary>
        [ClientRpc]
        private void PlayAttacking()
        {
            animator.SetTrigger(Attack);
        }

        /// <summary>
        ///     To call when the character takes damage
        /// </summary>
        private void PlayHurt()
        {
            animator.SetBool(IsHurt, true);
        }

        /// <summary>
        ///     To call when the character ends its invincibility time
        /// </summary>
        public void OnInvincibilityEnd()
        {
            animator.SetBool(IsHurt, false);
        }

        /// <summary>
        ///     Creates dust particles when the character walks, called from an animation
        /// </summary>
        public void CreateDustParticles()
        {
            if (createDustOnWalk)
            {
                dustParticleSystem.Stop();
                dustParticleSystem.Play();
            }
        }
    }
}