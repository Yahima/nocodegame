using UnityEngine;

namespace Character.Controllers
{
    /// <summary>
    ///     A contact enemy AI, this ai simply tries to go to the position of the nearest target under a
    ///     certain distance to touch it and deal damage via a NetChangeHealthOnTouch component.
    /// </summary>
    public class NetTopDownContactEnemyController : NetTopDownEnemyController
    {
        [SerializeField] 
        [Range(0f, 100f)]
        private float _followRange;

        protected override void FixedUpdate()
        {
            if (!isServer)
            {
                return;
            }
            
            base.FixedUpdate();

            Vector2 direction = Vector2.zero;
            if (DistanceToTarget() < _followRange)
            {
                direction = DirectionToTarget();
            }

            OnMoveEvent.Invoke(direction);
        }
    }
}