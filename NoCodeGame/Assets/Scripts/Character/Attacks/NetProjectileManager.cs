using Mirror;
using TopDownCharacter2D.Attacks.Range;
using UnityEngine;

namespace Character.Attacks
{
    public class NetProjectileManager : NetworkBehaviour
    {
        public static NetProjectileManager singleton;

        [Header("Settings")]
        [SerializeField] private GameObject _prefab;
        [SerializeField] private ParticleSystem _impactParticleSystem;

        [Header("Debug")]
        [SerializeField] private int _currentCount;
        
        private Pool<GameObject> _pool;

        private void Start()
        {
            InitializePool();
            singleton = this;
            NetworkClient.RegisterPrefab(_prefab, SpawnHandler, UnspawnHandler);
        }
        
        // used by NetworkClient.RegisterPrefab
        GameObject SpawnHandler(SpawnMessage msg) => Get(msg.position);

        // used by NetworkClient.RegisterPrefab
        void UnspawnHandler(GameObject spawned) => Return(spawned);
        
        void OnDestroy()
        {
            NetworkClient.UnregisterPrefab(_prefab);
        }

        private void InitializePool()
        {
            // create pool with generator function
            _pool = new Pool<GameObject>(CreateNew, 100);
        }

        private GameObject CreateNew()
        {
            // use this object as parent so that objects dont crowd hierarchy
            GameObject next = Instantiate(_prefab, transform);
            next.name = $"{_prefab.name}_pooled_{_currentCount}";
            next.SetActive(false);
            _currentCount++;
            return next;
        }

        // Used to take Object from Pool.
        // Should be used on server to get the next Object
        // Used on client by NetworkClient to spawn objects
        public GameObject Get(Vector3 position)
        {
            GameObject next = _pool.Get();

            // set position/rotation and set active
            next.transform.position = position;
            next.SetActive(true);
            return next;
        }

        // Used to put object back into pool so they can b
        // Should be used on server after unspawning an object
        // Used on client by NetworkClient to unspawn objects
        public void Return(GameObject spawned)
        {
            // disable object
            spawned.SetActive(false);

            // add back to pool
            _pool.Return(spawned);
        }

        [ClientRpc]
        public void CreateImpactParticlesAtPosition(Vector3 position, RangedAttackConfig config)
        {
            _impactParticleSystem.transform.position = position;
            
            var emissionModule = _impactParticleSystem.emission;
            emissionModule.SetBurst(0, new ParticleSystem.Burst(0, Mathf.Ceil(config.size * 5f)));
            
            var mainModule = _impactParticleSystem.main;
            mainModule.startSpeedMultiplier = config.size * 10f;
            
            _impactParticleSystem.Play();
        }
    }
}