using Character.Health;
using Mirror;
using TopDownCharacter2D.Attacks.Range;
using UnityEngine;

namespace Character.Attacks
{
    public class NetRangedAttackController : NetworkBehaviour
    {
        [Tooltip("The layer of the walls of the level")] 
        [SerializeField]
        private LayerMask levelCollisionLayer;
        
        private bool DestroyOnHit => true;

        private Rigidbody2D _rb;
        private TrailRenderer _trail;
        private SpriteRenderer _spriteRenderer;
        
        [SyncVar(hook = nameof(SyncConfig))]
        private RangedAttackConfig _config;
        
        [SyncVar]
        private bool _isReady;
        
        private Vector2 _direction;
        private uint _shooterNetId;
        
        private float _currentDuration;

        private void Awake()
        {
            _rb = GetComponent<Rigidbody2D>();
            _trail = GetComponent<TrailRenderer>();
            _spriteRenderer = GetComponent<SpriteRenderer>();
        }

        private void Update()
        {
            if (!_isReady || !isServer)
            {
                return;
            }
        
            _currentDuration += Time.deltaTime;
        
            if (_currentDuration > _config.duration)
            {
                DestroyProjectile(transform.position, false);
            }
        
            _rb.velocity = _direction * _config.speed;
        }
        
        private void OnTriggerEnter2D(Collider2D other)
        {
            if (!_isReady)
            {
                return;
            }
            
            if (!isServer)
            {
                return;
            }
            
            if (levelCollisionLayer.value == (levelCollisionLayer.value | (1 << other.gameObject.layer)))
            {
                if (DestroyOnHit)
                {
                    DestroyProjectile(other.ClosestPoint(transform.position) - _direction * .2f, true);
                }
            }
            else if (_config.target.value == (_config.target.value | (1 << other.gameObject.layer)))
            {
                NetHealthSystem health = other.gameObject.GetComponent<NetHealthSystem>();
                if (health != null && health.netId != _shooterNetId)
                {
                    health.ChangeHealth(-_config.power, transform.position);

                    if (DestroyOnHit)
                    {
                        DestroyProjectile(other.ClosestPoint(transform.position), true);
                    }
                }
            }
        }

        /// <summary>
        ///     Initializes the ranged attack with the given configuration
        /// </summary>
        /// <param name="direction"> The direction of the attack </param>
        /// <param name="config"> The parameters of the ranged attack</param>
        /// <param name="shooterNetId"> The NetId of the shooter</param>
        public void InitializeAttack(Vector2 direction, RangedAttackConfig config, uint shooterNetId)
        {
            _config = config;
            _direction = direction;
            _shooterNetId = shooterNetId;
            
            _currentDuration = 0f;
            transform.localScale = Vector3.one * _config.size;
            
            _trail.Clear();
            _spriteRenderer.color = config.projectileColor;
            
            _isReady = true;
        }

        private void SyncConfig(RangedAttackConfig oldValue, RangedAttackConfig newValue)
        {
            _trail.Clear();
            _spriteRenderer.color = newValue.projectileColor;
            // Debug.LogError($"Color = {newValue.projectileColor} | Size = {newValue.size}");
        }

        /// <summary>
        ///     Destroys the projectile
        /// </summary>
        /// <param name="pos">The position where to create the particles</param>
        /// <param name="createFx">Whether to create particles or not</param>
        [Server]
        private void DestroyProjectile(Vector2 pos, bool createFx)
        {
            if (createFx)
            {
                NetProjectileManager.singleton.CreateImpactParticlesAtPosition(pos, _config);
            }
            
            NetworkServer.UnSpawn(gameObject);
            NetProjectileManager.singleton.Return(gameObject);
        }
    }
}