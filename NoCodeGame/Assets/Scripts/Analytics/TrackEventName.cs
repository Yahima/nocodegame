namespace Analytics
{
    public enum TrackEventName
    {
        StartServer = 0,
        StartHost = 1,
        StartClient = 2,
    }
}