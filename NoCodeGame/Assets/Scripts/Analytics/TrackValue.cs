using System.Globalization;
using UnityEngine;

namespace Analytics
{
    public class TrackValue
    {
        int i_value = 0;
        long l_value = 0;
        float f_value = 0.0f;
        bool b_value = false;
        string s_value = "";
        private string[] a_value = new string[0];

        public enum HETVType
        {
            eUndefined = 0,
            eInt,
            eLong,
            eFloat,
            eBool,
            eString,
            eArray
        };

        HETVType myType = HETVType.eUndefined;

        public TrackValue(int value)
        {
            myType = HETVType.eInt;
            i_value = value;
        }

        public TrackValue(long value)
        {
            myType = HETVType.eLong;
            l_value = value;
        }

        public TrackValue(float value)
        {
            myType = HETVType.eFloat;
            f_value = value;
        }
        
        public TrackValue(bool value)
        {
            myType = HETVType.eBool;
            b_value = value;
        }

        public TrackValue(string value)
        {
            myType = HETVType.eString;
            s_value = value;
        }

        public TrackValue(string[] value)
        {
            myType = HETVType.eArray;
            a_value = value;
        }

        public new HETVType GetType()
        {
            return myType;
        }

        public int GetIValue()
        {
            int result = 0;

            if (myType != HETVType.eInt)
            {
                Debug.LogError("Error in TrackValue, wrong type");
            }
            else
            {
                result = i_value;
            }

            return result;
        }

        public long GetLValue()
        {
            long result = 0;

            if (myType != HETVType.eLong)
            {
                Debug.LogError("Error in TrackValue, wrong type");
            }
            else
            {
                result = l_value;
            }

            return result;
        }

        public float GetFValue()
        {
            float result = 0.0f;

            if (myType != HETVType.eFloat)
            {
                Debug.LogError("Error in TrackValue, wrong type");
            }
            else
            {
                result = f_value;
            }

            return result;
        }
        
        public bool GetBValue()
        {
            bool result = false;

            if (myType != HETVType.eBool)
            {
                Debug.LogError("Error in TrackValue, wrong type");
            }
            else
            {
                result = b_value;
            }

            return result;
        }

        public string GetSValue()
        {
            string result = "";

            if (myType != HETVType.eString)
            {
                Debug.LogError("Error in TrackValue, wrong type");
            }
            else
            {
                result = s_value;
            }

            return result;
        }

        public string[] GetAValue()
        {
            var result = new string[0];

            if (myType != HETVType.eArray)
            {
                Debug.LogError("Error in TrackValue, wrong type");
            }
            else
            {
                result = a_value;
            }

            return result;
        }

        public override string ToString()
        {
            switch (myType)
            {
                case HETVType.eInt:
                {
                    return i_value.ToString();
                }
                case HETVType.eLong:
                {
                    return l_value.ToString();
                }
                case HETVType.eBool:
                {
                    return b_value.ToString();
                }
                case HETVType.eFloat:
                {
                    return f_value.ToString(CultureInfo.InvariantCulture);
                }
                case HETVType.eArray:
                {
                    return a_value.ToString();
                }
            }

            return s_value;
        }
    }
}