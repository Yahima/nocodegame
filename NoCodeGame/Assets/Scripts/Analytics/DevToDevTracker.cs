using System;
using System.Collections.Generic;
using DevToDev.Analytics;

namespace Analytics
{
    public class DevToDevTracker
    {
        public void Init()
        {
            var config = GetConfig();
            
#if UNITY_ANDROID
            DTDAnalytics.Initialize("androidAppID");
#elif UNITY_IOS
            DTDAnalytics.Initialize("IosAppID");
#elif UNITY_WEBGL
            DTDAnalytics.Initialize("WebAppID");
#elif UNITY_STANDALONE_WIN
            DTDAnalytics.Initialize("winAppID");
#elif UNITY_STANDALONE_OSX
            DTDAnalytics.Initialize("0067a905-2fda-08a2-a860-4297e87ea034", config); //OsxAppId
#elif UNITY_WSA
            DTDAnalytics.Initialize("UwpAppID");
#endif
        }
        
        public void SendEvent(TrackEventName eventId, Dictionary<string, TrackValue> attributes)
        {
            var eventParams = CreateCustomEventParams(attributes);
            DTDAnalytics.CustomEvent(eventId.ToString(), eventParams);
        }

        private DTDAnalyticsConfiguration GetConfig()
        {
            return new DTDAnalyticsConfiguration
            {
                // ApplicationVersion = "1.2.3", //Windows only
                LogLevel = DTDLogLevel.No,
                TrackingAvailability = DTDTrackingStatus.Enable,
                // UserId = "unique_userId",
                // CurrentLevel = 1,
            };
        }
        
        private static DTDCustomEventParameters CreateCustomEventParams(Dictionary<string, TrackValue> attributes)
        {
            var eventParams = new DTDCustomEventParameters();
            foreach (var attribute in attributes)
            {
                switch (attribute.Value.GetType())
                {
                    case TrackValue.HETVType.eInt:
                        eventParams.Add(attribute.Key, attribute.Value.GetIValue());
                        break;
                    case TrackValue.HETVType.eLong:
                        eventParams.Add(attribute.Key, attribute.Value.GetLValue());
                        break;
                    case TrackValue.HETVType.eFloat:
                        eventParams.Add(attribute.Key, attribute.Value.GetFValue());
                        break;
                    case TrackValue.HETVType.eBool:
                        eventParams.Add(attribute.Key, attribute.Value.GetBValue());
                        break;
                    case TrackValue.HETVType.eString:
                        eventParams.Add(attribute.Key, attribute.Value.GetSValue());
                        break;
                    case TrackValue.HETVType.eArray:
                        break;
                    case TrackValue.HETVType.eUndefined:
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }

            return eventParams;
        }
    }
}