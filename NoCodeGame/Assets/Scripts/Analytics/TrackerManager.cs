using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Analytics
{
    public class TrackerManager : MonoBehaviour
    {
        public static TrackerManager Instance; // Экземпляр объекта
        
        private DevToDevTracker _tracker;

        private void Awake()
        {
            if (Instance != null)
            {
                Destroy(gameObject);
                return;
            }

            Instance = this;
            DontDestroyOnLoad(this);
        }

        private void Start()
        {
            _tracker = new DevToDevTracker();
            _tracker.Init();
        }

        private void ProcessEvent(TrackEventName eventId, Dictionary<string, TrackValue> parameters = null)
        {
            if (parameters == null)
            {
                parameters = new Dictionary<string, TrackValue>();
            }

#if UNITY_EDITOR || DEVELOPMENT_BUILD
            var paramLog = string.Join("\n", parameters.Select(x => $"{x.Key}: {x.Value}"));
            Debug.Log($"ProcessEvent {eventId}\n{paramLog}");
#endif

            _tracker.SendEvent(eventId, parameters);
        }

        public static EventBuilder Create(TrackEventName eventId)
        {
            return new EventBuilder(Instance, eventId);
        }
        
        public class EventBuilder
        {
            private const string None = "NONE";

            private readonly TrackEventName _event;
            private readonly Dictionary<string, TrackValue> _params = new Dictionary<string, TrackValue>();
            private readonly TrackerManager _trackerManager;

            internal EventBuilder(TrackerManager instance, TrackEventName name)
            {
                _event = name;
                _trackerManager = instance;
            }

            public EventBuilder AddAttribute(string name, int value)
            {
                _params.Add(name, new TrackValue(value));
                return this;
            }

            public EventBuilder AddAttribute(string name, long value)
            {
                _params.Add(name, new TrackValue(value));
                return this;
            }

            public EventBuilder AddAttribute(string name, float value)
            {
                _params.Add(name, new TrackValue(value));
                return this;
            }
            
            public EventBuilder AddAttribute(string name, bool value)
            {
                _params.Add(name, new TrackValue(value));
                return this;
            }

            public EventBuilder AddAttribute(string name, string value)
            {
                if (string.IsNullOrEmpty(value))
                {
                    value = None;
                }

                _params.Add(name, new TrackValue(value));
                return this;
            }

            public void Flush()
            {
                _trackerManager.ProcessEvent(_event, _params);
            }
        }
    }
}